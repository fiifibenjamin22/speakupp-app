//
//  SpeakUppEnums.swift
//  SpeakUpp
//
//  Created by Benjamin Acquah on 24/01/2018.
//  Copyright © 2018 Bright Limited. All rights reserved.
//




enum MessageType {
    case failed
    case success
    case warning
    case info
}

enum SearchType {
    case poll
    case people
    case brands
    case events
}


