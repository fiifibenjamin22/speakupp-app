//
//  Page.swift
//  SpeakUpp
//
//  Created by Benjamin Acquah on 22/01/2018.
//  Copyright © 2018 Bright Limited. All rights reserved.
//

struct Page {
    let imageName:String
    let title:String
    let message:String
}

struct HomeMenuLabel {
    let title:String
    let image:String
}

struct TrendingMenuLabel {
    let title:String
    let id:String
}

struct SearchMenuLabel {
    let title:String
    let type:SearchType
}

