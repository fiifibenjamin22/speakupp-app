//
//  ApiCallStatus.swift
//  SpeakUpp
//
//  Created by Benjamin Acquah on 05/02/2018.
//  Copyright © 2018 Bright Limited. All rights reserved.
//


enum ApiCallStatus {
    case FAILED
    case SUCCESS
    case WARNING
    case DETAIL
}

